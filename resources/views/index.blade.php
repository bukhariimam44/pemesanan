@extends('layouts.app')
@section('css')
@endsection
@section('content')
    <div class="row">
        <!-- Center colunm-->
        <div class="center_column col-xs-12 col-sm-12" id="center_column">
            <!-- category-slider -->
            <div class="category-slider">
                <ul class="owl-carousel owl-style2" data-dots="false" data-loop="true" data-nav = "true" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-items="1">
                    <li>
                        <img src="{{asset('storage/assets/images/sliders/slide1.jpg')}}" alt="category-slider">
                    </li>
                    <li>
                        <img src="{{asset('storage/assets/images/sliders/slide1.jpg')}}" alt="category-slider">
                    </li>
                </ul>
            </div>
            <!-- ./category-slider -->

            <!-- category short-description -->
            <!-- <div class="cat-short-desc">

                <div class="cat-short-desc-products">
                    <ul class="row">
                        <li class="col-sm-3">
                            <div class="product-container">
                                <div class="product-thumb">
                                <a href="#"><img src="{{asset('storage/assets/data/sub-cat1.jpg')}}" alt="Product"></a>
                                </div>
                                <h5 class="product-name">
                                    <a href="#">Sub category 1</a>
                                    <span>(90)</span>
                                </h5>
                            </div>
                        </li>
                        <li class="col-sm-3">
                            <div class="product-container">
                                <div class="product-thumb">
                                <a href="#"><img src="{{asset('storage/assets/data/sub-cat2.jpg')}}" alt="Product"></a>
                                </div>
                                <h5 class="product-name">
                                    <a href="#">Sub category 2</a>
                                    <span>(55)</span>
                                </h5>
                            </div>
                        </li>
                        <li class="col-sm-3">
                            <div class="product-container">
                                <div class="product-thumb">
                                <a href="#"><img src="{{asset('storage/assets/data/sub-cat3.jpg')}}" alt="Product"></a>
                                </div>
                                <h5 class="product-name">
                                    <a href="#">Sub category 3</a>
                                    <span>(100)</span>
                                </h5>
                            </div>
                        </li>
                        <li class="col-sm-3">
                            <div class="product-container">
                                <div class="product-thumb">
                                <a href="#"><img src="{{asset('storage/assets/data/sub-cat4.jpg')}}" alt="Product"></a>
                                </div>
                                <h5 class="product-name">
                                    <a href="#">Sub category 4</a>
                                    <span>(70)</span>
                                </h5>
                            </div>
                        </li>
                    </ul>
                </div>
            </div> -->
            <!-- ./category short-description -->
            <!-- view-product-list-->
            <div id="view-product-list" class="view-product-list">
                <h2 class="page-heading">
                    <span class="page-heading-title">Produk</span>
                </h2>
                <ul class="display-product-option">
                    <li class="view-as-grid selected">
                        <span>grid</span>
                    </li>
                </ul>
                <!-- PRODUCT LIST -->
                <ul class="row product-list style2 grid">
                    @foreach($produks as $p)
                    <li class="col-sx-12 col-sm-3">
                        <div class="product-container">
                            <div class="left-block">
                                <a href="{{route('detail',$p->id)}}">
                                    <img class="img-responsive" alt="product" src="{{asset('storage/produks/medium/'.$p->gambarArray[0]->gambar)}}" />
                                </a>
                                <div class="quick-view">
                                    <a title="Add to my wishlist" class="heart" href="#"></a>
                                    <a title="Add to compare" class="compare" href="#"></a>
                                    <a title="Quick view" class="search" href="{{route('detail',$p->id)}}"></a>
                                </div>
                            </div>
                            <div class="right-block">
                                <h5 class="product-name"><a href="#">{{$p->nama}}</a></h5>
                                <div class="product-star">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                </div>
                                <div class="content_price">
                                    <span class="price product-price">Rp {{number_format($p->harga,0,',','.')}}</span>
                                    <!-- <span class="price old-price">$52,00</span> -->
                                </div>
                                <div class="info-orther">
                                    <p>Item Code: #453217907</p>
                                    <p class="availability">Availability: <span>In stock</span></p>
                                    <div class="product-desc">
                                        Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien. Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium. Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor.
                                    </div>
                                </div>
                                <div class="add-to-cart">
                                    <a title="Detail" href="{{route('detail',$p->id)}}"><span></span>Detail</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                    
                </ul>
                <!-- ./PRODUCT LIST -->
            </div>
            <!-- ./view-product-list-->
            <div class="sortPagiBar">
                <div class="bottom-pagination">
                    <nav>
                        <ul class="pagination">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                            <span aria-hidden="true">Next &raquo;</span>
                            </a>
                        </li>
                        </ul>
                    </nav>
                </div>
                <div class="show-product-item">
                    <select name="">
                        <option value="">Show 18</option>
                        <option value="">Show 20</option>
                        <option value="">Show 50</option>
                        <option value="">Show 100</option>
                    </select>
                </div>
                <div class="sort-product">
                    <select>
                        <option value="">Product Name</option>
                        <option value="">Price</option>
                    </select>
                    <div class="sort-product-icon">
                        <i class="fa fa-sort-alpha-asc"></i>
                    </div>
                </div>
            </div>
        </div>
        <!-- ./ Center colunm -->
    </div>
@endsection
@section('js')
<script type="text/javascript" src="{{asset('storage/assets/js/jquery.arcticmodal.js')}}"></script>
@endsection