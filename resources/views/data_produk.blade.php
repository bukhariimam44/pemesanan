@extends('layouts.app')
@section('css')
<style>
.error{
    border-color:red;
}
</style>
@endsection
@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box-authentication">
                <h3>Data Produk <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#exampleModal">
  Input Produk
</button></h3> 
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead class="thead-dark">
                            <th><strong>No.</strong> </th><th><strong>Produk</strong></th><th><strong>Action</strong></th>
                        </thead>
                        <tbody>
                            @foreach($datas as $key =>$data)
                            <tr>
                                <td>{{$key+1}}.</td>
                                <td>{{$data['warna']}}</td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="form">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('data-produk')}}" method="post" enctype="multipart/form-data"  id="simpanproduk">
            <input type="hidden" name="action" value="add">
        {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="">Nama Barang</label>
                    <input type="text" name="nama" id="" class="form-control @error('nama') error @enderror" placeholder="@error('nama'){{ $message }}@enderror">
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Kode Barang</label>
                    <input type="text" name="kode" id="" class="form-control @error('kode') error @enderror" placeholder="@error('kode'){{ $message }}@enderror">
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Brand</label>
                    <select name="brand" id="" class="form-control">
                        @foreach($brands as $dt)
                        <option value="{{$dt['brand']}}">{{$dt['brand']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Warna</label>
                    <select name="warna" id="" class="form-control">
                        @foreach($warnas as $warna)
                        <option value="{{$warna['warna']}}">{{$warna['warna']}} <span style="background:{{$warna['kode']}};">&nbsp;&nbsp;&nbsp;&nbsp;</span></option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Ukuran</label>
                    <select name="ukuran" id="" class="form-control">
                        @foreach($ukurans as $ukuran)
                        <option value="{{$ukuran['ukuran']}}">{{$ukuran['ukuran']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Jenis Barang</label>
                    <select name="jenis" id="" class="form-control">
                        @foreach($jenis as $j)
                        <option value="{{$j['jenis']}}">{{$j['jenis']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4 form-group">
                    <label for="">Gambar 1</label><br>
                    <img :src="gambar1" width="100px" alt="">
                    <input type="file" class="form-control @error('gambar') error @enderror" name="gambar[]" accept="image/*" @change="uploadGambar1">
                </div>
                <div class="col-md-4 form-group">
                    <label for="">Gambar 2</label><br>
                    <img :src="gambar2" width="100px" alt="">
                    <input type="file" class="form-control @error('gambar') error @enderror" name="gambar[]" accept="image/*" @change="uploadGambar2">
                </div>
                <div class="col-md-4 form-group">
                    <label for="">Gambar 3</label><br>
                    <img :src="gambar3" width="100px" alt="">
                    <input type="file" class="form-control @error('gambar') error @enderror" name="gambar[]" accept="image/*" @change="uploadGambar3">
                </div>
                <div class="col-md-12 form-group">
                    <label for="">Penjelasan </label> 
                    <textarea name="penjelasan" id="" cols="30" rows="4" class="form-control @error('penjelasan') error @enderror" placeholder="@error('penjelasan'){{ $message }}@enderror"></textarea>
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Harga  </label> 
                    <input type="text" name="harga" id="" class="form-control @error('harga') error @enderror" placeholder="@error('harga'){{ $message }}@enderror">
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Potongan Harga Sub Agen</label>
                    <input type="text" name="diskon_subagen" id="" class="form-control @error('diskon_subagen') error @enderror" placeholder="@error('diskon_subagen'){{ $message }}@enderror">
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Potongan Harga Member</label>
                    <input type="text" name="diskon_member" id="" class="form-control @error('diskon_member') error @enderror" placeholder="@error('diskon_member'){{ $message }}@enderror">
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Potongan Harga Reseller</label>
                    <input type="text" name="diskon_reseller" id="" class="form-control @error('diskon_reseller') error @enderror" placeholder="@error('diskon_reseller'){{ $message }}@enderror">
                </div>
                <div class="col-md-12 form-group">
                    <label for="">Ukuran Tersedia</label><br>
                    @foreach($ukurans as $ukr)
                    <label><input type="checkbox" name="ukuran_tersedia[]" value="{{$ukr['ukuran']}}"> {{$ukr['ukuran']}}</label>&nbsp;&nbsp;&nbsp;&nbsp;
                    @endforeach
                </div>
                <div class="col-md-12 form-group">
                    <label for="">Warna Terdia :</label> <br>
                    @foreach($warnas as $war)
                    <label><input type="checkbox" name="warna_tersedia[]" value="{{$war['warna']}}"> <span style="background:{{$war['kode']}};">&nbsp;&nbsp;&nbsp;&nbsp;</span> {{$war['warna']}}</label>&nbsp;&nbsp;&nbsp;&nbsp;
                    @endforeach
                    
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" @click="simpan()">Simpan Data</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
var form = new Vue({
    el:'#form',
    data: {
        gambar1:'',
        gambar2:'',
        gambar3:''
    },
    methods: {
        loading(item){
            Swal.fire({
                title: item,
                allowEscapeKey: false,
                allowOutsideClick: false,
                background: '#FFFFFF',
                showConfirmButton: false,
                onOpen: ()=>{
                                Swal.showLoading();
                }
            }).then((dismiss) => {
                // Swal.showLoading();
                }
            );
        },
        uploadGambar1(e){
            const image = e.target.files[0]
            const reader = new FileReader()
            reader.readAsDataURL(image)
            reader.onload = e => {
                this.gambar1 = e.target.result
            }
        },
        uploadGambar2(e){
            const image = e.target.files[0]
            const reader = new FileReader()
            reader.readAsDataURL(image)
            reader.onload = e => {
                this.gambar2 = e.target.result
            }
        },
        uploadGambar3(e){
            const image = e.target.files[0]
            const reader = new FileReader()
            reader.readAsDataURL(image)
            reader.onload = e => {
                this.gambar3 = e.target.result
            }
        },
        simpan(){
            this.loading('Mohon menunggu')
            document.getElementById('simpanproduk').submit();
        }
    },
});
</script>
@endsection