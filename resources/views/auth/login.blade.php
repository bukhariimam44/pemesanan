@extends('layouts.app')
@section('css')
@endsection
@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box-authentication">
                <h3>Login</h3>
                <form action="{{route('login')}}" method="post">
                {{ csrf_field() }}
                    <label for="emmail_login">Email</label>
                    <input id="emmail_login" type="text" name="email" class="form-control">
                    <label for="password_login">Password</label>
                    <input id="password_login" type="password" name="password" class="form-control">
                    <p class="forgot-pass"><a href="#">Lupa kata sandi Anda?</a></p>
                    <button class="button"><i class="fa fa-lock"></i> Login</button>
                </form>
                
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@endsection