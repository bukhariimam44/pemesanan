@extends('layouts.app')
@section('css')
@endsection
@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="box-authentication">
                <h3>Daftar Akun Baru</h3>
                <!-- <p>Please enter your email address to create an account.</p> -->

                <label for="emmail_register">Nama Lengkap</label>
                <input type="text" class="form-control">

                <label for="emmail_register">Email (Aktif)</label>
                <input type="text" class="form-control">

                <label for="emmail_register">Nomor WA (Aktif)</label>
                <input type="text" class="form-control">   

                <label for="">Password</label>
                <input type="password" class="form-control">        

                <label for="">Level</label>
                <select name="level" id="" class="form-control">
                    @foreach($levels as $l)
                    <option value="{{$l['level']}}">{{$l['level']}}</option>
                    @endforeach
                </select>        

                
                <label for="flexCheckDefault">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"> Setujui, Syarat dan Ketentuan berlaku.
                </label> <br><br>
                <button class="button"><i class="fa fa-user"></i> D A F T A R</button>
                
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@endsection