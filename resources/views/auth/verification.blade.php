@extends('layouts.app')
@section('css')
@endsection
@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box-authentication">
                <h3>Verifikasi Email</h3>
                <p>Masukan kode verifikasi yang kami kirim ke email kamu dibawah.</p>
                <form action="{{route('verification.notice')}}" method="post">
                {{ csrf_field() }}
                    <input type="text" name="kode" class="form-control" placeholder="Kode Verifikasi">
                    <button class="button"><i class="fa fa-lock"></i> Proses</button>
                </form>
                
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@endsection