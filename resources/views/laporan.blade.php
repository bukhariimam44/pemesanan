@extends('layouts.app')
@section('css')
@endsection
@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box-authentication">
                <h3>Laporan</h3>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead class="thead-dark">
                            <th><strong>No.</strong> </th><th><strong>Laporan</strong></th><th><strong>Action</strong></th>
                        </thead>
                        <tbody>
                            @foreach($datas as $key =>$data)
                            <tr>
                                <td>{{$key+1}}.</td>
                                <td>{{$data['warna']}}</td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@endsection