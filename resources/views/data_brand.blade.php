@extends('layouts.app')
@section('css')
@endsection
@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box-authentication">
                <h3>Data Brand <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#exampleModal">
  Tambah
</button></h3>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead class="thead-dark">
                            <th><strong>No.</strong> </th><th><strong>Brand</strong></th><th><strong>Action</strong></th>
                        </thead>
                        <tbody>
                            @foreach($brands as $key =>$brand)
                            <tr>
                                <td>{{$key+1}}.</td>
                                <td>{{$brand['brand']}}</td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="form">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Brand</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('data-brand')}}" method="post" id="simpanproduk">
            <input type="hidden" name="action" value="add">
        {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12 form-group">
                    <label for="">Brand</label>
                    <input type="text" name="brand" class="form-control @error('brand') error @enderror" placeholder="@error('brand'){{ $message }}@enderror">
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" @click="simpan()">Simpan Data</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
@endsection