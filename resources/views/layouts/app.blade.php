<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{asset('storage/assets/lib/bootstrap/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('storage/assets/lib/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('storage/assets/lib/select2/css/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('storage/assets/lib/jquery.bxslider/jquery.bxslider.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('storage/assets/lib/owl.carousel/owl.carousel.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('storage/assets/lib/jquery-ui/jquery-ui.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('storage/assets/css/animate.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('storage/assets/css/reset.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('storage/assets/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('storage/assets/css/responsive.css')}}" />
    @yield('css')
    <title>{{config('app.name')}}</title>
</head>
<body class="category-page right-sidebar">
<!-- HEADER -->
            
@include('components.header')
<!--  Popup Newsletter-->
<div class="modal fade popup-newsletter" id="popup-newsletter" tabindex="-1" role="dialog" >
                <div class="modal-dialog" role="document">
                    <div class="modal-content" style="background-image: url(images/media/index1/Popup.jpg);">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="block-newletter">
                            <div class="block-title">signup for our newsletter & promotions</div>
                            <div class="block-content">
                                <p class="text-title">GET <span>50% <span>off</span></span></p>
                                <form>
                                    <label>on your next purchase</label>
                                    <div class="input-group">
                                        <input type="text" placeholder="Enter your email..." class="form-control">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-subcribe"><span>Subscribe</span></button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                            <div class="checkbox btn-checkbox">
                                <label>
                                    <input type="checkbox"><span>Dont’s show this popup again!</span>
                                </label>
                            </div>
                        </div>

                        
                        
                    </div>
                </div>
            </div><!--  Popup Newsletter-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        @include('components.breadcrumb')
        <!-- ./breadcrumb -->
        <!-- row -->
        @yield('content')
        <!-- ./row-->
    </div>
</div>

<!-- Footer -->
@include('components.footer')

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="{{asset('storage/assets/lib/jquery/jquery-1.11.2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('storage/assets/lib/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('storage/assets/lib/select2/js/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('storage/assets/lib/jquery.bxslider/jquery.bxslider.min.js')}}"></script>
<script type="text/javascript" src="{{asset('storage/assets/lib/owl.carousel/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('storage/assets/lib/jquery.countdown/jquery.countdown.min.js')}}"></script>

<script type="text/javascript" src="{{asset('storage/assets/lib/jquery-ui/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('storage/assets/js/jquery.actual.min.js')}}"></script>
<!-- COUNTDOWN -->
<script type="text/javascript" src="{{asset('storage/assets/lib/countdown/jquery.plugin.js')}}"></script>
<script type="text/javascript" src="{{asset('storage/assets/lib/countdown/jquery.countdown.js')}}"></script>
<!-- ./COUNTDOWN -->
@yield('js')
<script type="text/javascript" src="{{asset('storage/assets/js/theme-script.js')}}"></script>

</body>
</html>