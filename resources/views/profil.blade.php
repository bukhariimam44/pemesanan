@extends('layouts.app')
@section('css')
@endsection
@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box-authentication">
                <label for="">Nama : </label> <p>- {{Auth::user()->name}} </p><br>
                <label for="">Email : </label> <p>- {{Auth::user()->email}} </p><br>
                <label for="">Nomor HP/WA : </label><p>- {{Auth::user()->phone}} </p><br>
                <label for="">Level : </label><p>- {{Auth::user()->level}} </p><br>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@endsection