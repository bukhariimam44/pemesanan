<footer id="footer">
<?php $kontak = App\Models\Kontak::find(1);?>
     <div class="container">
            <!-- introduce-box -->
            <div id="introduce-box" class="row">
                <div class="col-md-4">
                    <div id="address-box">
                        <a href="#"><img src="{{asset('storage/assets/images/logo.png')}}" alt="" /></a>
                        <div id="address-list">
                            <div class="tit-name">Alamat:</div>
                            <div class="tit-contain">{{$kontak['alamat']}}</div>
                            <div class="tit-name">Telpon:</div>
                            <div class="tit-contain">{{$kontak['telp']}}</div>
                            <div class="tit-name">Email:</div>
                            <div class="tit-contain">{{$kontak['email']}}</div>
                        </div>
                    </div> 
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="introduce-title">Brand</div>
                            <ul id="introduce-company"  class="introduce-list">
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Testimonials</a></li>
                                <li><a href="#">Affiliate Program</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <div class="introduce-title">Menu</div>
                            <ul id = "introduce-Account" class="introduce-list">
                                <li><a href="#">My Order</a></li>
                                <li><a href="#">My Wishlist</a></li>
                                <li><a href="#">My Credit Slip</a></li>
                                <li><a href="#">My Addresses</a></li>
                                <li><a href="#">My Personal In</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div id="contact-box">
                        <div class="introduce-title">Newsletter</div>
                        <div class="input-group" id="mail-box">
                          <input type="text" placeholder="Your Email Address"/>
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="button">OK</button>
                          </span>
                        </div><!-- /input-group -->
                        <div class="introduce-title">Let's Socialize</div>
                        <div class="social-link">
                            <a href="{{$kontak['facebook']}}"><i class="fa fa-facebook"></i></a>
                            <a href="{{$kontak['instagram']}}"><i class="fa fa-instagram"></i></a>
                            <a href="{{$kontak['twitter']}}"><i class="fa fa-twitter"></i></a>
                            <a href="{{$kontak['google']}}"><i class="fa fa-google-plus"></i></a>
                        </div>
                    </div>
                    
                </div>
            </div><!-- /#introduce-box -->
        
            <!-- #trademark-box -->
            <div id="trademark-box" class="row">
                <div class="col-sm-12">
                    <ul id="trademark-list">
                        <li id="payment-methods">Accepted Payment Methods</li>
                        <li>
                            <a href="#"><img src="{{asset('storage/assets/images/bni.png')}}" width="70px"  alt="ups"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="{{asset('storage/assets/images/bca.png')}}" width="70px"  alt="ups"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="{{asset('storage/assets/images/bri.png')}}"  width="70px" alt="ups"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="{{asset('storage/assets/images/mandiri.png')}}"  width="70px" alt="ups"/></a>
                        </li>
                    </ul> 
                </div>
            </div> <!-- /#trademark-box -->
            
            <div id="footer-menu-box">
                <p class="text-center">Copyrights &#169; 2021 {{config('app.name')}}. All Rights Reserved. Designed by Imam Bukhari</p>
            </div><!-- /#footer-menu-box -->
        </div> 
</footer>