<div id="header" class="header">
    <?php $kontak = App\Models\Kontak::find(1);?>
    <div class="top-header">
        <div class="container">
            <div class="nav-top-links">
                <a class="first-item" href="#"><img alt="phone" src="{{asset('storage/assets/images/phone.png')}}" />{{$kontak['telp']}}</a>
                <a href="mailto:{{$kontak['email']}}"><img alt="email" src="{{asset('storage/assets/images/email.png')}}" />Hubungi kami hari ini!</a>
            </div>
            <div class="support-link">
                @if(Auth::check())
                <a href="{{route('keluar')}}">Keluar</a>
                @else
                <a href="{{route('login')}}">Login</a>
                <a href="{{route('daftar')}}">Daftar</a>
                @endif
                <a href="#"></a><a href="#"></a>
            </div>
        </div>
    </div>
    <!--/.top-header -->
    <!-- MAIN HEADER -->
    <div class="container main-header">
        <div class="row">
            <div class="col-xs-12 col-sm-3 logo">
                <a href="{{route('index')}}"><img alt="#" src="{{asset('storage/assets/images/logo.png')}}" /></a>
            </div>
            <div class="col-xs-12 col-sm-9 header-search-box">
                <form class="form-inline">
                      <div class="form-group form-category">
                        <select class="select-category">
                            <option value="">Semua</option>
                            <option value="Dewasa">Dewasa</option>
                            <option value="Anak-Anak">Anak-Anak</option>
                        </select>
                      </div>
                      <div class="form-group input-serach">
                        <input type="text"  placeholder="Cari produk apa... ?">
                      </div>
                      <button type="submit" class="pull-right btn-search"></button>
                </form>
            </div>
        </div>
    </div>
    <!-- END MANIN HEADER -->
    <div id="nav-top-menu" class="nav-top-menu">
        <div class="container">
            <div class="row">
                <div class="col-sm-3" id="box-vertical-megamenus">
                    <div class="box-vertical-megamenus">
                    <h4 class="title">
                        <span class="title-menu">Brand</span>
                        <span class="btn-open-mobile pull-right"><i class="fa fa-bars"></i></span>
                    </h4>
                    <div class="vertical-menu-content is-home">
                        <ul class="vertical-menu-list">
                            @foreach($data = App\Models\Brand::get() as $key => $b)
                            <li @if($key > 10) class="cat-link-orther" @endif><a href="{{url('/?brand='.$b['brand'])}}"><img class="icon-menu" alt="Funky roots" src="{{asset('storage/assets/data/5.png')}}">{{$b->brand}}</a></li>
                            @endforeach
                        </ul>
                        @if(count($data) > 10)
                        <div class="all-category"><span class="open-cate">All Categories</span></div>
                        @endif
                    </div>
                </div>
                </div>
                <div id="main-menu" class="col-sm-9 main-menu">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <a class="navbar-brand" href="#">MENU</a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li class="{{setActive(['index','login','register','detail'])}}"><a href="{{route('index')}}">Produk</a></li>
                                    <li class="{{setActive(['syarat'])}}"><a href="{{route('syarat')}}">Syarat</a></li>
                                    <li class="{{setActive(['kontak'])}}"><a href="{{route('kontak')}}">Kontak</a></li>
                                    @if(Auth::check())
                                        @if(Auth::user()->level == 'Agen')
                                        <li class="dropdown {{setActive(['data-user','data-brand','jenis-barang','data-warna','data-produk'])}}">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Data Master</a>
                                            <ul class="dropdown-menu container-fluid">
                                                <li class="block-container">
                                                    <ul class="block">
                                                        <li class="link_container"><a href="{{route('data-user')}}">Data User</a></li>
                                                        <li class="link_container"><a href="{{route('data-brand')}}">Data Brand</a></li>
                                                        <li class="link_container"><a href="{{route('jenis-barang')}}">Jenis Barang </a></li>
                                                        <li class="link_container"><a href="{{route('data-warna')}}">Data Warna</a></li>
                                                        <li class="link_container"><a href="{{route('data-produk')}}">Data Produk</a></li>
                                                    </ul>
                                                </li>
                                            </ul> 
                                        </li>
                                        <li class="{{setActive(['data-pesanan'])}}"><a href="{{route('data-pesanan')}}">Data Pesanan</a></li>
                                        <li class="{{setActive(['laporan'])}}"><a href="{{route('laporan')}}">Laporan</a></li>
                                        @else
                                        <li class="{{setActive(['data-orderan'])}}"><a href="{{route('data-orderan')}}">Orderan</a></li>
                                        @endif
                                    @endif
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </nav>
                </div>
            </div>
            <!-- userinfo on top-->
            <div id="form-search-opntop">
            </div>
            <!-- userinfo on top-->
            <div id="user-info-opntop">
            </div>
            <!-- CART ICON ON MMENU -->
            <div id="shopping-cart-box-ontop">
                <i class="fa fa-shopping-cart"></i>
                <div class="shopping-cart-box-ontop-content"></div>
            </div>
        </div>
    </div>
</div>