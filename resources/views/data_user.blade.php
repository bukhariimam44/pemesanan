@extends('layouts.app')
@section('css')
<style>
.error{
    border-color:red;
}
</style>
@endsection
@section('content')
<div class="page-content" id="form">
    <div class="row">
        <div class="col-sm-12">
            <div class="box-authentication">
                <h3>Data User <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#exampleModal">
  Input User
</button></h3>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead class="thead-dark">
                            <th><strong>No.</strong> </th><th><strong>Nama</strong></th><th><strong>Email</strong></th><th><strong>Nomor HP/WA</strong></th><th><strong>Level</strong></th>
                        </thead>
                        <tbody>
                            @foreach($users as $key =>$user)
                            <tr>
                                <td>{{$key+1}}.</td>
                                <td>{{$user['name']}}</td>
                                <td>{{$user['email']}}</td>
                                <td>{{$user['phone']}}</td>
                                <td style="background-color:@if($user['level']=='Agen') #31e631 @elseif($user['level']=='Member') #31e6d5 @elseif($user['level']=='Sub Agen') #dbe631 @elseif($user['level']=='Reseller') #fdb8f8 @endif">{{$user['level']}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Produk</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="{{route('data-user')}}" method="post"  id="simpanuser">
                <input type="hidden" name="action" value="add">
            {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="">Nama</label>
                        <input type="text" name="nama" id="" class="form-control @error('nama') error @enderror" placeholder="@error('nama'){{ $message }}@enderror">
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="">Email</label>
                        <input type="text" name="email" id="" class="form-control @error('email') error @enderror" placeholder="@error('email'){{ $message }}@enderror">
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="">Phone</label>
                        <input type="text" name="phone" id="" class="form-control @error('phone') error @enderror" placeholder="@error('phone'){{ $message }}@enderror">
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="">Password</label>
                        <input type="text" name="password" id="" class="form-control @error('password') error @enderror" placeholder="@error('password'){{ $message }}@enderror">
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="">Level</label>
                        <select name="level" id="" class="form-control">
                            @foreach($levels as $j)
                            <option value="{{$j['level']}}">{{$j['level']}}</option>
                            @endforeach
                        </select>
                    </div>
                    
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" @click="simpan()">Simpan Data</button>
        </div>
        </div>
    </div>
    </div>
</div>

@endsection
@section('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
var form = new Vue({
    el:'#form',
    data() {
        return {
            
        }
    },
    methods: {
        loading(item){
            Swal.fire({
                title: item,
                allowEscapeKey: false,
                allowOutsideClick: false,
                background: '#FFFFFF',
                showConfirmButton: false,
                onOpen: ()=>{
                                Swal.showLoading();
                }
            }).then((dismiss) => {
                // Swal.showLoading();
                }
            );
        },
        simpan(){
            this.loading('Mohon menunggu')
            document.getElementById('simpanuser').submit();
        }
    },
});
</script>
@endsection