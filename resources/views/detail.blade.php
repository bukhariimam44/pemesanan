@extends('layouts.app')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('storage/assets/lib/fancyBox/jquery.fancybox.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('storage/assets/lib/jquery.bxslider/jquery.bxslider.css')}}" />
@endsection
@section('content')
<div class="row">
            
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!-- Product -->
                    <div id="product">
                        <div class="primary-box row">
                            <div class="pb-left-column col-xs-12 col-sm-5">
                                <!-- product-imge-->
                                <div class="product-image">
                                    <div class="product-full">
                                        <img id="product-zoom" 
                                        src="{{asset('storage/produks/medium/'.$datas['gambarArray'][0]['gambar'])}}" 
                                        data-zoom-image="{{asset('storage/produks/large/'.$datas['gambarArray'][0]['gambar'])}}"/>
                                    </div>
                                    <div class="product-img-thumb" id="gallery_01">
                                        <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="21" data-loop="true">
                                            @foreach($datas['gambarArray'] as $dt)
                                            <li>
                                                <a href="#" 
                                                data-image="{{asset('storage/produks/medium/'.$dt['gambar'])}}" 
                                                data-zoom-image="{{asset('storage/produks/large/'.$dt['gambar'])}}">
                                                    <img id="product-zoom"  src="{{asset('storage/produks/small/'.$dt['gambar'])}}" /> 
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <!-- product-imge-->
                            </div>
                            <div class="pb-right-column col-xs-12 col-sm-7">
                                <h1 class="product-name">{{$datas['nama']}}</h1>
                                <div class="product-comments">
                                    <div class="product-star">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                    <!-- <div class="comments-advices">
                                        <a href="#">Based  on 3 ratings</a>
                                        <a href="#"><i class="fa fa-pencil"></i> write a review</a>
                                    </div> -->
                                </div>
                                <div class="product-price-group">
                                    <span class="price">Rp {{number_format($datas['harga'],0,',','.')}}</span>
                                    <span class="old-price">Rp {{number_format($datas['harga'] + ($datas['harga']*20/100),0,',','.')}}</span>
                                    <!-- <span class="discount">-30%</span> -->
                                </div>
                                <div class="info-orther">
                                    <p>Kode Item: {{$datas['kode']}}</p>
                                    <p>Warna: {{$datas['warna']}}</p>
                                    <p>Kondisi: Baru</p>
                                </div>
                                <div class="product-desc">
                                {!! nl2br($datas['penjelasan']) !!}
                                </div>
                                <div class="form-option">
                                    <p class="form-option-title">Tersedia:</p>
                                    <div class="attributes">
                                        <div class="attribute-label">Warna:</div>
                                        <div class="attribute-list">
                                            <ul class="list-color">
                                                <li style="background:#0c3b90;"></li>
                                                <li style="background:#036c5d;"></li>
                                                <li style="background:#5f2363;"></li>
                                                <li style="background:#ffc000;"></li>
                                                <li style="background:#36a93c;"></li>
                                                <li style="background:#ff0000;"></li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                    <div class="attributes">
                                        <div class="attribute-label">Ukuran:</div>
                                        <div>
                                                <span>X</span>
                                                <span>XL</span>
                                                <span>XXL</span>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="form-action">
                                    <div class="button-group">
                                        <a class="btn-add-cart" href="#">Pesan Sekarang</a>
                                    </div>
                                    <div class="button-group">
                                        <a class="wishlist" href="#"><i class="fa fa-heart-o"></i>
                                        <br>Wishlist</a>
                                        <a class="compare" href="#"><i class="fa fa-signal"></i>
                                        <br>        
                                        Compare</a>
                                    </div>
                                </div>
                                <div class="form-share">
                                    <div class="sendtofriend-print">
                                        <a href="javascript:print();"><i class="fa fa-print"></i> Print</a>
                                        <a href="#"><i class="fa fa-envelope-o fa-fw"></i>Send to a friend</a>
                                    </div>
                                    <div class="network-share">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                <!-- Product -->
            </div>
            <!-- ./ Center colunm -->
        </div>
@endsection
@section('js')
<script type="text/javascript" src="{{asset('storage/assets/lib/jquery.elevatezoom.js')}}"></script>
<script type="text/javascript" src="{{asset('storage/assets/lib/fancyBox/jquery.fancybox.js')}}"></script>

@endsection