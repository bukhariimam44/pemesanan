<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;

class HomeController extends Controller
{
    public function simpanGambar($imageName,$path,$request){
        $destinationPath = public_path('/storage/'.$path);
        $request->move(public_path('storage/'.$path.'/original'), $imageName);
        $data = getimagesize(public_path('storage/'.$path.'/original/'.$imageName));
        $large_width = 850;
        $large_height = 1036;
        $medium_width = 420;
        $medium_height = 512;
        $small_width = 100;
        $small_height = 122;
        $img = Image::make($destinationPath.'/original/'.$imageName);
        
        $img->resize($large_width,$large_height, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/large/'.$imageName);

        $img->resize($medium_width,$medium_height, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/medium/'.$imageName);

        $img->resize($small_width,$small_height, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/small/'.$imageName);

        return $imageName;
    }
}
