<?php

namespace App\Http\Controllers;

use App\Http\Controllers\HomeController as HomeController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Level;
use App\Models\Product;
use App\Models\GambarProduk;
use App\Models\User;
use App\Models\Brand;
use App\Models\JenisBarang;
use App\Models\Warna;
use App\Models\UkuranDewasa;
use App\Models\Pemesanan;
use Illuminate\Support\Str;
use Auth;
use DB;
use Log;

class LoginController extends HomeController
{
    public function index(Request $request){
        $breadcrumb = 'Busana';
        
        $produks = Product::where('brand','LIKE','%'.$request->brand.'%')->get();
        return view('index',compact('breadcrumb','produks'));
    }
    public function login(Request $request){
        $breadcrumb = 'Login';
        if ($request->email && $request->password) {
            $userdata = array(
                'email'  => $request->email,
                'password'  => $request->password
            );
            $message = [
                'email.required'=>'Email harus diisi',
                'email.email'=>'Email tidak falid',
                'password.min'=>'Password Minimal 6',
            ];
            $response = array('response' => '', 'success'=>false);
            $request->validate([
                'email' => 'required|email',
                'password'=>'required|min:6'
            ],$message);
            if (Auth::attempt($userdata)) {
                return redirect()->route('profil');
            }
        }
        return view('auth.login',compact('breadcrumb'));
    }
    public function daftar(Request $request){
        $breadcrumb = 'Daftar';
        $levels = Level::where('level','<>','Agen')->orderBy('created_at','ASC')->get();
        return view('auth.register',compact('breadcrumb','levels'));
    }
    public function syarat(Request $request){
        $breadcrumb = 'Syarat & ketentuan';
        return view('syarat',compact('breadcrumb'));
    }
    public function detail(Request $request,$id){
        $breadcrumb = 'Detail Produk';
        $datas = Product::find($id);
        return view('detail',compact('breadcrumb','datas'));
    }
    public function kontak(Request $request){
        $breadcrumb = 'Kontak';
        return view('kontak',compact('breadcrumb'));
    }
    //PEMESANAN
    public function pemesanan(Request $request){
        $breadcrumb = 'Pemesanan';
        return view('pemesanan',compact('breadcrumb'));
    }
    //Profil
    public function profil(Request $request){
        $breadcrumb = 'Profil';
        return view('profil',compact('breadcrumb'));
    }
    public function verification(Request $request){
        $breadcrumb = 'Verifikasi Email';
        if ($request->kode && $request->user()->kode == $request->kode) {
            $update = User::where('kode',$request->kode)->first()->update([
                'email_verified_at'=>date('Y-m-d H:i:s')
            ]);
            return redirect()->route('profil');
        }elseif ($request->user()->email_verified_at != null) {
            return redirect()->route('profil');
        }
        return view('auth.verification',compact('breadcrumb'));
    }
    public function data_pesanan(Request $request){
        $breadcrumb = 'Data Pesanan';
        return view('data_pesanan',compact('breadcrumb'));
    }
    public function data_user(Request $request){
        $breadcrumb = 'Data User';
        if ($request->action == 'add') {
            $request->validate([
                'nama' => 'required',
                'email'=>'required',
                'phone' => 'required',
                'level' => 'required',
                'password' => 'required'
            ],[
                'nama.required'=>'Wajib diisi',
                'email.required'=>'Wajib diisi',
                'phone.required'=>'Wajib diisi',
                'level.required'=>'Wajib diisi',
                'password.required'=>'Wajib diisi'
            ]);
            $add = User::create([
                'name'=>$request->nama,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'level'=>$request->level,
                'open'=>1,
                'password'=>Hash::make($request->password),
                'kode'=>Str::random(6)
            ]);
            if ($add) {
                return redirect()->back();
            }
            return redirect()->back();
        }
        $users = User::where('open',1)->get();
        $levels = Level::orderBy('created_at','ASC')->get();
        return view('data_user',compact('breadcrumb','users','levels'));
    }
    public function data_brand(Request $request){
        $breadcrumb = 'Data Brand';
        $brands = Brand::where('open',1)->get();
        return view('data_brand',compact('breadcrumb','brands'));
    }
    public function jenis_barang(Request $request){
        $breadcrumb = 'Data Jenis Barang';
        $datas = JenisBarang::where('open',1)->get();
        return view('jenis_barang',compact('breadcrumb','datas'));
    }
    public function data_warna(Request $request){
        $breadcrumb = 'Data Warna';
        $datas = Warna::where('open',1)->get();
        return view('data_warna',compact('breadcrumb','datas'));
    }
    public function data_produk(Request $request){
        $breadcrumb = 'Data Produk';
        if ($request->action == 'add') {
            $request->validate([
                'nama' => 'required',
                'kode'=>'required',
                'brand' => 'required',
                'warna' => 'required',
                'ukuran' => 'required',
                'jenis' => 'required',
                'harga' => 'required',
                'penjelasan' => 'required',
                'diskon_subagen' => 'required',
                'diskon_member' => 'required',
                'diskon_reseller' => 'required',
            ],[
                'nama.required'=>'Wajib diisi',
                'kode.required'=>'Wajib diisi',
                'brand.required'=>'Wajib diisi',
                'warna.required'=>'Wajib diisi',
                'ukuran.required'=>'Wajib diisi',
                'jenis.required'=>'Wajib diisi',
                'harga.required'=>'Wajib diisi',
                'penjelasan.required'=>'Wajib diisi',
                'diskon_subagen.required'=>'Wajib diisi',
                'diskon_member.required'=>'Wajib diisi',
                'diskon_reseller.required'=>'Wajib diisi',
            ]);
            DB::beginTransaction();
            try {
                $add = Product::create([
                    'nama'=>$request->nama,
                    'kode'=>$request->kode,
                    'harga'=>$request->harga,
                    'penjelasan'=>$request->penjelasan,
                    'ukuran_dewasa'=>$request->ukuran,
                    'jenis_barang'=>$request->jenis,
                    'warna'=>$request->warna,
                    'brand'=>$request->brand,
                    'discon_subagen'=>$request->diskon_subagen,
                    'discon_member'=>$request->diskon_member,
                    'discon_reseller'=>$request->diskon_reseller,
                    'open'=>1
                ]);
                $files=$request->gambar;
                foreach ($files as $key => $value) {
                    $imageName = time().rand(100,999).'.'.$value->extension();
                    $name = $this->simpanGambar($imageName,'produks',$value);
                    $photo = GambarProduk::create([
                        'product_id'=>$add->id,
                        'gambar'=>$name
                    ]);
                }
                $name;
            } catch (\Throwable $th) {
                Log::info($th);
                DB::rollback();
                return redirect()->back();
            }
            DB::commit();
        }
        $datas = Product::where('open',1)->get();
        $brands = Brand::where('open',1)->get();
        $warnas = Warna::where('open',1)->get();
        $ukurans = UkuranDewasa::where('open',1)->get();
        $jenis = JenisBarang::where('open',1)->get();
        return view('data_produk',compact('breadcrumb','datas','brands','warnas','ukurans','jenis'));
    }
    public function laporan(Request $request){
        $breadcrumb = 'Laporan';
        $datas = Pemesanan::get();
        return view('laporan',compact('breadcrumb','datas'));
    }
    public function keluar(){
        Auth::logout();
        return redirect()->route('index');
    }
    public function data_orderan(Request $request){
        $breadcrumb = 'Orderan';
        return view('data_orderan',compact('breadcrumb'));
    }
}
