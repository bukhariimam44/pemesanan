<?php
use App\Models\User;
use Illuminate\Support\Facades\Log;

function setActive($uri, $output = "active")
{
  if (is_array($uri)) {
    foreach ($uri as $u) {
      if (Route::is($u)) {
        return $output;
      }
    }
  } else {
    if (Route::is($uri)) {
      return $output;
    }
  }
}
function userPhoto(User $user, $attributes = [])
{
    return Html::image(
        userPhotoPath($user->photoUserId['small'] ? $user->photoUserId['small'] : userPhotoIcon($user->profileId['gender_id'] ? $user->profileId['gender_id'] : 1), $user->profileId['gender_id'] ? $user->profileId['gender_id'] : 1),
        null,
        $attributes
    );
}
function userPhotoIcon($genderId)
{
    return asset('images/icon_user_'.$genderId.'.png');
}
function userPhotoPath($photoPath, $genderId)
{
  return checkUrl('users','small',$photoPath);
    // if (is_file(public_path('storage/users/'.$photoPath))) {
    //     return asset('users/'.$photoPath);
    // }

    // return asset('images/icon_user_'.$genderId.'.png');
}

function userPhotoAgent($photoPath, $lokasi){
  return asset('users/'.$lokasi.'/'.$photoPath);
}

function allPhoto($photo,$lokasi){
  $data = [
    "ID"=> $photo['id'],
    "Original"=>checkUrl($lokasi,$path = 'origininal',$photo['original']),//asset($lokasi.'/original/'.$photo->original),
    "Medium"=>checkUrl($lokasi,$path = 'medium',$photo['medium']),//asset($lokasi.'/medium/'.$photo->medium),
    "Small"=>checkUrl($lokasi,$path = 'small',$photo['small']),//asset($lokasi.'/small/'.$photo->small)
  ];
  return $data;
}
function allPhotoArray($photo,$lokasi){
  $data = [];
  Log::info('PhotoArray = '.json_encode($photo));
  if ($photo) {
    foreach ($photo as $key => $value) {
      $data[] = [
        "ID"=> $value->id,
        "Original"=> checkUrl($lokasi,$path = 'origininal',$value->original),//asset($lokasi.'/original/'.$value->original),
        "Medium"=>checkUrl($lokasi,$path = 'medium',$value->medium),//asset($lokasi.'/medium/'.$value->medium),
        "Small"=>checkUrl($lokasi,$path = 'small',$value->small),//asset($lokasi.'/small/'.$value->small)
      ];
    }
  }
  
  
  return $data;
}
function checkUrl($lokasi, $path, $url){
  $ex = explode(':',$url,4);
  if ($ex[0] == 'http' || $ex[0] == 'https') {
    return $url;
  }
  return asset($lokasi.'/'.$path.'/'.$url);
}
function video($name){
  return asset('video/'.$name);
}