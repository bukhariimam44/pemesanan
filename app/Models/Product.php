<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\GambarProduk;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'nama',
        'kode','harga','penjelasan','ukuran_dewasa','jenis_barang','warna','brand','discon_subagen','discon_member','discon_reseller','open'
    ];
    public function gambarArray(){
        return $this->HasMany(GambarProduk::class, 'product_id');
    }
}
