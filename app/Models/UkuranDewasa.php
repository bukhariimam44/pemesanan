<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UkuranDewasa extends Model
{
    use HasFactory;
    protected $fillable = [
        'ukuran',
        'open'
    ];
}
