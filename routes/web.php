<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [LoginController::class, 'index'])->name('index');
Route::get('/login', [LoginController::class, 'login'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'login'])->name('login')->middleware('guest');
Route::get('/daftar', [LoginController::class, 'daftar'])->name('daftar')->middleware('guest');
Route::get('/syarat-dan-ketentuan', [LoginController::class, 'syarat'])->name('syarat');
Route::get('/detail-produk/{id}', [LoginController::class, 'detail'])->name('detail');
Route::get('/kontak', [LoginController::class, 'kontak'])->name('kontak');
Route::get('/verification', [LoginController::class, 'verification'])->name('verification.notice')->middleware('auth');
Route::post('/verification', [LoginController::class, 'verification'])->name('verification.notice')->middleware('auth');
Route::get('/keluar', [LoginController::class, 'keluar'])->name('keluar')->middleware('auth');
Route::middleware(['auth','verified'])->group( function () {
    Route::get('/profil', [LoginController::class, 'profil'])->name('profil');
    Route::get('/pemesanan', [LoginController::class, 'pemesanan'])->name('pemesanan')->middleware(['subagen','member','reseller']);;
    Route::get('/data-user', [LoginController::class, 'data_user'])->name('data-user')->middleware('agen');
    Route::post('/data-user', [LoginController::class, 'data_user'])->name('data-user')->middleware('agen');
    Route::get('/data-brand', [LoginController::class, 'data_brand'])->name('data-brand')->middleware('agen');
    Route::get('/data-jenis-barang', [LoginController::class, 'jenis_barang'])->name('jenis-barang')->middleware('agen');
    Route::get('/data-warna', [LoginController::class, 'data_warna'])->name('data-warna')->middleware('agen');
    Route::get('/data-produk', [LoginController::class, 'data_produk'])->name('data-produk')->middleware('agen');
    Route::post('/data-produk', [LoginController::class, 'data_produk'])->name('data-produk')->middleware('agen');
    Route::get('/data-pesanan', [LoginController::class, 'data_pesanan'])->name('data-pesanan')->middleware('agen');
    Route::get('/laporan', [LoginController::class, 'laporan'])->name('laporan')->middleware('agen');

   
    //NONO AGEN
    Route::get('/data-orderan', [LoginController::class, 'data_orderan'])->name('data-orderan')->middleware('nonagen');

});
